﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        //TODO: Add Unit Tests
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var limitRequest = new SetPartnerPromoCodeLimitRequest
            {
                EndDate = DateTime.Today.AddDays(7),
                Limit = 5
            };

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitRequest);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            partner.IsActive = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var limitRequest = new SetPartnerPromoCodeLimitRequest
            {
                EndDate = DateTime.Today.AddDays(7),
                Limit = 5
            };

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }


        [Theory]
        [InlineData(0)]
        public async void SetPartnerPromoCodeLimitAsync_LimitZero_ReturnsBadRequest(int limit)
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var limitRequest = new SetPartnerPromoCodeLimitRequest
            {
                EndDate = DateTime.Today.AddDays(7),
                Limit = limit
            };

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_BeforeLimitNoEnd_NumberIssuedPromoCodesZero()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            partner.NumberIssuedPromoCodes = 100;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var limitRequest = new SetPartnerPromoCodeLimitRequest
            {
                EndDate = DateTime.Today.AddDays(7),
                Limit = 100
            };

            var activeLimitBefore = partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue);
            activeLimitBefore.EndDate = DateTime.Today.AddDays(7);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitRequest);
            var result = partner.NumberIssuedPromoCodes;

            // Assert
            result.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_BeforeLimitEnd_NumberIssuedPromoCodesNoChanged()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            partner.NumberIssuedPromoCodes = 100;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var limitRequest = new SetPartnerPromoCodeLimitRequest
            {
                EndDate = DateTime.Today.AddDays(7),
                Limit = 100
            };

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitRequest);
            var result = partner.NumberIssuedPromoCodes;

            // Assert
            result.Should().Be(100);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_BeforeLimitCancel_ReturnsTrue()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();


            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var limitRequest = new SetPartnerPromoCodeLimitRequest
            {
                EndDate = DateTime.Today.AddDays(7),
                Limit = 100
            };

            var activeLimitBefore = partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue);


            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitRequest);
            var result = partner.PartnerLimits.FirstOrDefault(x => x.Id == activeLimitBefore.Id).CancelDate;

            // Assert
            result.Should().NotBeNull();
        }
    }
}


